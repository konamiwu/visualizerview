package com.konami.visualizerview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    private lateinit var visualizerView: VisualizerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        visualizerView = findViewById(R.id.visualizerView)
    }

    fun action(view: View) {
        visualizerView.targetProgress += .1f
    }
}
