package com.konami.visualizerview

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.animation.*
import kotlin.math.sqrt


class VisualizerView : View {

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        val typeArray = context!!.theme.obtainStyledAttributes(
            attrs,
            R.styleable.visualizer,
            0, 0
        )

        progressWidth = typeArray.getDimension(R.styleable.visualizer_progressWidth, 0f)
        outArrowSize = typeArray.getDimension(R.styleable.visualizer_outArrowWidth, 0f) * 2 / sqrt(3f)
        innerArrowSize = typeArray.getDimension(R.styleable.visualizer_innerArrowWidth, 0f) * 2 / sqrt(3f)
    }

    var cornerRadius = 6f

    var sectionNum = 12
        set(value) {
            field = value
            invalidate()
        }

    var dividerHeight = 2f
        set(value) {
            field = value
            dividerPaint.strokeWidth = value
        }

    var dividerPadding = 12f
        set(value) {
            field = value
            invalidate()
        }

    var dividerColor = Color.WHITE
        set(value) {
            field = value
            dividerPaint.color = value
        }

    var targetProgress: Float = 0.0f
        set(value) {
            field = if (value > 1)
                1f
            else
                value

            if (value > progress) {
                downAnimator?.removeAllListeners()
                downAnimator?.removeAllUpdateListeners()
                upAnimator?.removeAllUpdateListeners()
                upAnimator?.removeAllListeners()
                upTime = System.currentTimeMillis()
                valueAnimator?.removeAllUpdateListeners()
                valueAnimator?.removeAllListeners()
                peekProgress = 0f
                isAdding = true
                val valueAnimator = ValueAnimator.ofFloat(progress, field)
                valueAnimator.duration = 500
                valueAnimator.interpolator = AccelerateDecelerateInterpolator()
                valueAnimator.addUpdateListener {
                    val newProgress = it.animatedValue as Float
                    progress = newProgress
                }
                valueAnimator.addListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(p0: Animator?) {
                    }

                    override fun onAnimationEnd(p0: Animator?) {
                        valueAnimator?.removeAllUpdateListeners()
                        valueAnimator?.removeAllListeners()
                        isAdding = false
                        down()
                    }

                    override fun onAnimationCancel(p0: Animator?) {
                    }

                    override fun onAnimationStart(p0: Animator?) {
                    }
                })
                valueAnimator.start()
            }
        }

    var progress: Float = 0.0f
        set(value) {
            field = value
            invalidate()
        }

    var progressColor = Color.rgb(255, 195, 195)
    var progressWidth = 0f

    var accSpeed = 10

    var speed = 2

    var outArrowColor = Color.WHITE
        set(value) {
            field = value
            invalidate()
        }

    var innerArrowColor = Color.rgb(255, 195, 195)
        set(value) {
            field = value
            invalidate()
        }

    var outArrowSize = 30f
        set(value) {
            field = value
            invalidate()
        }

    var innerArrowSize = 10f
        set(value) {
            field = value
            invalidate()
        }

    var arrowPosition = 0

    private val dividerPaint = Paint()
    private val progressPaint = Paint()
    private val backgroundPaint = Paint()
    private val peekPaint = Paint()
    private val outArrowPaint = Paint()
    private var isAdding = false
    private var valueAnimator: ValueAnimator? = null
    private var downAnimator: ValueAnimator? = null
    private var upAnimator: ValueAnimator? = null
    private var peekProgress = 0f
    private var upTime = System.currentTimeMillis()
    init {
        setLayerType(LAYER_TYPE_SOFTWARE, null)
        dividerPaint.isAntiAlias = true
        dividerPaint.color = dividerColor
        dividerPaint.strokeWidth = dividerHeight

        backgroundPaint.color = Color.argb(127, 245, 245, 245)
        backgroundPaint.isAntiAlias = true

        progressPaint.color = Color.rgb(255, 195, 195)
        progressPaint.isAntiAlias = true

        peekPaint.color = Color.RED
        peekPaint.isAntiAlias = true

        outArrowPaint.color = Color.WHITE
        outArrowPaint.isAntiAlias = true
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (canvas != null) {
            drawBackground(canvas)
            drawProgress(canvas)
            drawPeek(canvas)
            drawDivider(canvas)
            drawArrow(canvas)
        }

//        if (!isAdding && progress > 0) {
//            var newHeight = (height * progress - speed)
//
//            if (newHeight < 0) {
//                newHeight = 0f
//            }
//            progress = newHeight / height
//            postInvalidate()
//        }
    }

    private fun down() {
        downAnimator = ValueAnimator.ofFloat(progress, progress - 0.1f)
        downAnimator!!.duration = 500
        downAnimator!!.interpolator = LinearInterpolator()
        downAnimator!!.addUpdateListener {
            val newProgress = it.animatedValue as Float
            progress = newProgress
        }
        downAnimator!!.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {
            }

            override fun onAnimationEnd(p0: Animator?) {
                if (progress > 0) {
                    downAnimator!!.removeAllListeners()
                    downAnimator!!.removeAllUpdateListeners()
                    up()
                }
            }

            override fun onAnimationCancel(p0: Animator?) {
            }

            override fun onAnimationStart(p0: Animator?) {
            }
        })
        downAnimator!!.start()
    }

    private fun up() {
        upAnimator = ValueAnimator.ofFloat(progress, progress + 0.05f)
        upAnimator!!.duration = 500
        upAnimator!!.interpolator = LinearInterpolator()
        upAnimator!!.addUpdateListener {
            val newProgress = it.animatedValue as Float
            progress = newProgress
        }
        upAnimator!!.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {
            }

            override fun onAnimationEnd(p0: Animator?) {
                if (progress > 0) {
                    upAnimator!!.removeAllListeners()
                    upAnimator!!.removeAllUpdateListeners()
                    down()
                }
            }

            override fun onAnimationCancel(p0: Animator?) {
            }

            override fun onAnimationStart(p0: Animator?) {
            }
        })
        upAnimator!!.start()
    }

    private fun drawBackground(canvas: Canvas) {
        val rect = RectF(width - progressWidth, 0f, width.toFloat(), height.toFloat())
        canvas.drawRoundRect(rect, cornerRadius, cornerRadius, backgroundPaint)
    }

    private fun drawDivider(canvas: Canvas) {
        val space = height / sectionNum

        for (i in 1 until sectionNum) {
            canvas.drawLine(
                width - progressWidth.toInt() + dividerPadding,
                (space * i).toFloat(),
                width - dividerPadding,
                (space * i).toFloat(),
                dividerPaint
            )
        }
    }

    private fun drawProgress(canvas: Canvas) {
        val rect = RectF(width - progressWidth, height * (1f - progress), width.toFloat(), height.toFloat())
        canvas.drawRoundRect(rect, cornerRadius, cornerRadius, progressPaint)
    }

    private fun drawPeek(canvas: Canvas) {
        if (peekProgress < progress)
            peekProgress = progress

        val y = height * (1f - peekProgress)
        val rect = RectF(width - progressWidth, y, width.toFloat(), y + 8)
        canvas.drawRoundRect(rect, cornerRadius, cornerRadius, peekPaint)
    }

    private fun drawArrow(canvas: Canvas) {
        val path = Path()
        path.moveTo(0f, height.toFloat())
        path.rLineTo(0f, -outArrowSize)
        path.rLineTo(outArrowSize * sqrt(3f) / 2, outArrowSize / 2)
        path.rLineTo(-outArrowSize * sqrt(3f) / 2, outArrowSize / 2)

        path.close()
        path.fillType = Path.FillType.EVEN_ODD
        canvas.drawPath(path, outArrowPaint)
    }
}